<?php

declare(strict_types=1);

namespace PagerWave\Extension\DoctrineDbal\Tests;

use Doctrine\DBAL\DriverManager;
use PagerWave\Extension\DoctrineDbal\QueryBuilderAdapter;
use PagerWave\Extension\DoctrineDbal\Tests\Fixtures\EntityDefinition;
use PagerWave\Query;
use PHPUnit\Framework\TestCase;

/**
 * @covers \PagerWave\Extension\DoctrineDbal\QueryBuilderAdapter
 */
class QueryBuilderAdapterTest extends TestCase
{
    private const TEST_SCHEMA = <<<EOSQL
CREATE TABLE entities (
    id INTEGER NOT NULL,
    ranking INTEGER NOT NULL
)
EOSQL;

    /**
     * @var \Doctrine\DBAL\Connection
     */
    private $database;

    protected function setUp(): void
    {
        $driverOptions = \PHP_VERSION_ID >= 80100
            ? [\PDO::ATTR_STRINGIFY_FETCHES => true]
            : [];

        $this->database = DriverManager::getConnection([
            'url' => 'sqlite:///:memory:',
            'driverOptions' => $driverOptions,
        ]);

        if (\is_callable([$this->database, 'executeStatement'])) {
            $this->database->executeStatement(self::TEST_SCHEMA);
        } else {
            /** @noinspection PhpDeprecationInspection */
            $this->database->exec(self::TEST_SCHEMA);
        }

        $this->database->insert('entities', ['ranking' => 3, 'id' => 8]);
        $this->database->insert('entities', ['ranking' => 1, 'id' => 800]);
        $this->database->insert('entities', ['ranking' => 3, 'id' => 6]);
        $this->database->insert('entities', ['ranking' => 10, 'id' => 12]);
        $this->database->insert('entities', ['ranking' => 4, 'id' => 9]);
        $this->database->insert('entities', ['ranking' => 2, 'id' => 10]);
    }

    protected function tearDown(): void
    {
        $this->database->close();
    }

    public function testPaging(): void
    {
        $qb = $this->database->createQueryBuilder()
            ->select('id', 'ranking')
            ->from('entities');

        $adapter = new QueryBuilderAdapter($qb);
        $results = $adapter->getResults(5, new EntityDefinition(), new Query());

        $this->assertSame(
            ['12', '9', '6', '8', '10'],
            array_map(function ($v) {
                return (string) $v; // PHP 8.1 fix
            }, array_column($results->getEntries(), 'id'))
        );
        $this->assertSame('800', (string) $results->getNextEntry()['id']);
    }

    public function testPagingWithQuery(): void
    {
        $qb = $this->database->createQueryBuilder()
            ->select('id', 'ranking')
            ->from('entities');

        $adapter = new QueryBuilderAdapter($qb);
        $results = $adapter->getResults(5, new EntityDefinition(), new Query([
            'ranking' => 2,
            'id' => 10,
        ]));

        $this->assertSame(
            ['10', '800'],
            array_map(function ($v) {
                return (string) $v; // PHP 8.1 fix
            }, array_column($results->getEntries(), 'id'))
        );
        $this->assertNull($results->getNextEntry());
    }

    public function testCannotInstantiateWithWrongBuilderType(): void
    {
        $qb = $this->database->createQueryBuilder()->update('entities');

        $this->expectException(\InvalidArgumentException::class);
        new QueryBuilderAdapter($qb);
    }
}

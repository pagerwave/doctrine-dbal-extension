<?php

declare(strict_types=1);

namespace PagerWave\Extension\DoctrineDbal\Tests\Fixtures;

use PagerWave\DefinitionGroupTrait;
use PagerWave\DefinitionInterface;

class EntityDefinition implements DefinitionInterface
{
    use DefinitionGroupTrait;

    public function getFieldNames(): array
    {
        return ['ranking', 'id'];
    }

    public function isFieldDescending(string $fieldName): bool
    {
        return $fieldName === 'ranking';
    }
}

# pagerwave/doctrine-dbal-extension

This package lets you paginate [Doctrine DBAL][dbal] query builders using
[PagerWave][pagerwave].

## Installation

~~~
$ composer require pagerwave/doctrine-dbal-extension
~~~

## Usage

~~~php
use PagerWave\Extension\DoctrineDbal\QueryBuilderAdapter;

$queryBuilder = $conn->createQueryBuilder()
    ->select('a.*')
    ->from('articles', 'a');

$adapter = new QueryBuilderAdapter($queryBuilder);
~~~

Read the [PagerWave documentation][docs] to learn more.

## Licence

This project is released under the Zlib licence.


[dbal]: https://www.doctrine-project.org/projects/dbal.html
[docs]: https://gitlab.com/pagerwave/PagerWave/-/tree/1.x/docs
[pagerwave]: https://gitlab.com/pagerwave/PagerWave

# Change log

## v1.3.0 (2023-01-11)

* Allow PHP 8.2.
* Fix failing tests under PHP 8.1 or later.

## v1.2.0 (2021-11-30)

* Allow PHP 8.1.
* Avoid calling deprecated DBAL methods.
* Drop support for PagerWave 1.x.

## v1.1.0 (2020-11-29)

* Allow PHP 8.0.
* Allow Doctrine DBAL 3.x.

## v1.0.0 (2020-09-28)

* Split off from main PagerWave repository.

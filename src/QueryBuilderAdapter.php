<?php

declare(strict_types=1);

namespace PagerWave\Extension\DoctrineDbal;

use Doctrine\DBAL\Query\Expression\ExpressionBuilder;
use Doctrine\DBAL\Query\QueryBuilder;
use PagerWave\Adapter\AdapterInterface;
use PagerWave\AdapterResult;
use PagerWave\AdapterResultInterface;
use PagerWave\DefinitionInterface;
use PagerWave\QueryInterface;

/**
 * Paginates a Doctrine DBAL {@link QueryBuilder}.
 *
 * The field names from the definition *MUST NOT* come from user input, or you
 * risk SQL injection!
 */
final class QueryBuilderAdapter implements AdapterInterface
{
    /**
     * @var QueryBuilder
     */
    private $queryBuilder;

    /**
     * @var int[]|string[]|null
     */
    private $typeMap;

    public function __construct(QueryBuilder $queryBuilder, array $typeMap = [])
    {
        if ($queryBuilder->getType() !== QueryBuilder::SELECT) {
            throw new \InvalidArgumentException('Builder must be SELECT type');
        }

        $this->queryBuilder = clone $queryBuilder;
        $this->typeMap = $typeMap;
    }

    public function getResults(
        int $max,
        DefinitionInterface $definition,
        QueryInterface $query
    ): AdapterResultInterface {
        $qb = clone $this->queryBuilder;
        $qb->setMaxResults($max + 1);

        foreach ($definition->getFieldNames() as $field) {
            $desc = $definition->isFieldDescending($field);

            if ($query->isFilled()) {
                $elements[] = [$field, ":next_$field", $desc];
                $type = $this->typeMap[$field] ?? null;

                $qb->setParameter("next_$field", $query->get($field), $type);
            }

            $qb->addOrderBy($field, $desc ? 'DESC' : 'ASC');
        }

        if (isset($elements)) {
            $this->mangleQuery($qb, $elements);
        }

        if (\is_callable([$qb, 'executeQuery'])) {
            $results = $qb->executeQuery()->fetchAllAssociative();
        } else {
            /** @noinspection PhpDeprecationInspection */
            $results = $qb->execute()->fetchAll();
        }

        $pagerEntity = \count($results) > $max ? array_pop($results) : null;

        return new AdapterResult($results, $pagerEntity);
    }


    /**
     * Add a where-clause to the query like:
     *
     * ~~~
     * (a <= 3) AND (a < 3 OR b >= 4) AND (a < 3 AND b > 4 OR c <= 5)
     * ~~~
     *
     * @param array $elements [field, value, is descending]
     */
    private function mangleQuery(QueryBuilder $qb, array $elements): void
    {
        $oldDbal = !\method_exists(ExpressionBuilder::class, 'and');
        $i = 0;

        $expr = $qb->expr()->{$oldDbal ? 'andX' : 'and'}(...array_map(
            static function ($field) use ($elements, $oldDbal, $qb, &$i) {
                $prev = array_map(static function ($field) use ($qb) {
                    return $qb->expr()->{$field[2] ? 'lt' : 'gt'}($field[0], $field[1]);
                }, array_slice($elements, 0, $i++));

                if ($prev) {
                    $expr[] = $qb->expr()->{$oldDbal ? 'andX' : 'and'}(...$prev);
                }

                $expr[] = $qb->expr()->{$field[2] ? 'lte' : 'gte'}($field[0], $field[1]);

                return $qb->expr()->{$oldDbal ? 'orX' : 'or'}(...$expr);
            },
            $elements
        ));

        $qb->andWhere($expr);
    }
}
